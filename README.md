# Hithub Project

Hithub is a web application that displays details about the top 50 USA songs on Spotify, including information about the songs themselves, the artists, genres, and various associated attributes.

## Project Structure

The project is structured into two main components organized into the `frontend` and `backend` folders:

### Frontend (React)

The frontend of Hithub is built using React, a popular JavaScript library for building user interfaces. It provides a responsive and interactive user experience for browsing and viewing details about the top songs.

### Backend (Flask, PostgreSQL, SQLAlchemy)

The backend of Hithub utilizes Flask, a lightweight and versatile web framework for Python. It interfaces with a PostgreSQL database using SQLAlchemy, an ORM (Object-Relational Mapping) library for Python. This setup allows efficient storage, retrieval, and management of song data fetched from Spotify.

## Features

- **Display Top 50 USA Songs**: Hithub fetches and displays the latest information about the top 50 songs in the USA from Spotify.
- **Song Details**: Provides comprehensive details about each song, including its title, duration, artist(s), genre(s), and other attributes.
- **Artist Information**: Offers insights into the artists behind the songs, such as their names, popular works, and genres.
- **Genre Classification**: Classifies songs into genres and provides information about each genre represented in the top 50 list.
- **Search Functionality**: Allows users to search for specific songs or artists to quickly access detailed information.

## Getting Started

To run the Hithub project locally, follow these steps:

### Prerequisites

- Node.js and npm installed for the frontend.
- Python 3.x, Flask, PostgreSQL, and SQLAlchemy installed for the backend. Install these through the `requirements.txt` folder as such: 
```bash 
pip install -r requirements.txt
```

### Installation

1. **Clone the repository:**

   ```bash
   $ git clone https://github.com/<your-username>/hithub.git
   $ cd hithub
   ```