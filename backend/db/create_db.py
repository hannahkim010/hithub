import json
import ast
import pandas as pd
import requests
from db.models import db, app, Song, Artist, Genre

def load_json(filename):
  with open(filename) as file:
    jsn = json.load(file)
    file.close()
  
  return jsn

def clean_album_json(json_str):
  lst = json_str.split()
  new_json_str = ''
  for i in range(27):
    new_json_str += lst[i]
  new_json_str = new_json_str[:len(new_json_str) - 1] + '}'
  
  return new_json_str.replace("'", '"')

def condense_bio(bio):
  sentences = bio.split('.')
  
  return '{sent1}. {sent2}'.format(sent1 = sentences[0].strip(), sent2 = (sentences[1].strip() + '.') if len(sentences) > 1 and sentences[1] != '' else '')
  
def create_songs():
  csv = pd.read_csv('./db/data/top50.csv')
  csv['duration_ms'] = csv['duration_ms'].apply(lambda x : '{min:2.0f}:{sec:2.0f}'.format(min = x / 1000/ 60, sec = x / 1000 % 60))
  csv['duration_ms'] = csv['duration_ms'].apply(lambda x : '{min}:{sec}'.format(min = x[:2], sec = x[3:].replace(' ', '0')))
  csv['artists'] = csv['artists'].apply(lambda x : json.loads(x.replace("'", '"'))[0]['name'])
  csv['album'] = csv['album'].apply(lambda x : json.loads(clean_album_json(x))['images'][0]['url'])
  csv['external_urls'] = csv['external_urls'].apply(lambda x : json.loads(x.replace("'", '"'))['spotify'])
  songs = pd.DataFrame([csv['rank'], csv['name'], csv['duration_ms'], csv['artists'], csv['album'], csv['external_urls']]).T
  for index, row in songs.iterrows():
    artist = db.session.query(Artist).filter_by(name = row['artists']).one()
    record = Song(rank = row['rank'], name = row['name'], duration = row['duration_ms'], image = row['album'], 
        spotify_page = row['external_urls'], artist = artist, genre = artist.genre
    )
    db.session.add(record)
  db.session.commit()
    
def create_artists():
  csv = pd.read_csv('./db/data/artists.csv')
  csv['followers'] = csv['followers'].apply(lambda x : int(json.loads(x.replace("'", '"').replace('None', 'null'))['total']))
  csv['bio'] = csv['bio'].apply(lambda x : None if pd.isna(x) else condense_bio(x))
  csv['images'] = csv['images'].apply(lambda x : str(json.loads(x.replace("'", '"'))[0]['url']))
  csv['external_urls'] = csv['external_urls'].apply(lambda x : json.loads(x.replace("'", '"'))['spotify'])
  csv['genres'] = csv['genres'].apply(lambda x : ast.literal_eval(x))
  artists = pd.DataFrame([csv['name'], csv['popularity'], csv['followers'], csv['bio'], csv['images'], csv['external_urls'], csv['genres']]).T
  for index, row in artists.iterrows():
    record = Artist(name = row['name'], popularity = row['popularity'], followers = row['followers'], spotify_page = row['external_urls'],
        biography = row['bio'], image = row['images'], genre = db.session.query(Genre).filter_by(name = row['genres'][0]).one())
    db.session.add(record)
  db.session.commit()

def create_genres():
  genres = pd.read_csv('./db/data/genres.csv')
  for index, row in genres.iterrows():
    record = Genre(name = row['name'], description = row['description'], origin_date = row['origin_date'], origin_location = row['origin_location'], )
    db.session.add(record)
  db.session.commit()

def delete_artists():
  db.session.query(Artist).filter_by(songs = None).delete()
  db.session.commit()
  artists = db.session.query(Artist).all()
  new_id = 1
  for artist in artists:
    artist.id = new_id
    new_id += 1
  db.session.commit()

def delete_genres():
  db.session.query(Genre).filter_by(artists = None, songs = None).delete()
  db.session.commit()
  genres = db.session.query(Genre).all()
  new_id = 1
  for genre in genres:
    genre.id = new_id
    new_id += 1
  db.session.commit()

def create_youtube():
  youtube_api_base_url = 'https://www.googleapis.com/youtube/v3/'
  youtube_api_key = 'AIzaSyBJc4UbiPsPy5YMzqoR_N1hEEeNTIG4FBk'
  video_id_lst = []
  for genre in db.session.query(Genre).all():
    youtube_url = f'{youtube_api_base_url}search?key={youtube_api_key}&type=video&maxResults=1&q=what+is+{genre.name}+music'
    vid_data = requests.get(youtube_url).json()
    video_id_lst.append(vid_data["items"][0]["id"]["videoId"])
  ser = pd.Series(video_id_lst, name = 'video_id')
  ser.to_csv('./data/youtube.csv', header = True)

def insert_youtube():
  youtube = pd.read_csv('./db/data/youtube.csv')
  i = 0
  for genre in db.session.query(Genre).all():
    genre.video_id = youtube.loc[i]['video_id']
    i += 1
  db.session.commit()

create_genres()
create_artists() 
create_songs()
delete_artists()
delete_genres()
# create_youtube()
insert_youtube()