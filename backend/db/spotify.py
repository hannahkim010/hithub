"""
spotify.py will query the top 50 songs in the USA under the Spotify playlist. After finding these
songs, the function will then query to find the associated artists and their data through 
wikipedia. The genre attributes are powered through OpenAI gpt-3.5-turbo-16k.

special notes: 
    - in order to run this file, you will need to create an OpenAI project and obtain the 
        secret access key. save that as an environment variable as OPENAI_SECRET_KEY
    - the spotify client id and client secret are available openly on this repository because
        these apis are free of charge to use
"""

import os
import json
import requests
import wikipediaapi
import pandas as pd
import pickle

accounts_base_url = 'https://accounts.spotify.com'
spotify_api_base_url = 'https://api.spotify.com/v1'
spotify_client_id = '79ef3ea71075497d9b372f3f6206929a'
spotify_client_secret = '0774d80af7dd403982f55c542537c281'
openai_api_base_url = 'https://api.openai.com/v1/chat/completions'
openai_api_secret_key = os.getenv('OPENAI_SECRET_KEY')
if not openai_api_secret_key:
    raise Exception('Secret key for openai not found under environment variable OPENAI_SECRET_KEY!')

def _get_auth_token():
    auth_url = f'{accounts_base_url}/api/token'

    headers = { 
        'Content-Type': 'application/x-www-form-urlencoded' 
    }

    data = {
        'grant_type': 'client_credentials',
        'client_id': spotify_client_id,
        'client_secret': spotify_client_secret
    }

    resp = requests.post(auth_url, headers=headers, data=data)
    return resp.json()['access_token']

def _get_genres(authorization_token, genre_name_list):
    print('getting genre data...')

    genre_name_list = genre_name_list
    messages = [
        {
            'role' : 'user',
            'content' : 'please answer these following questions. ' + \
                'separate each question i give with the special character "|" and also ONLY give the answer. ' + \
                'do not repeat the question in the response! ' + \
                'there should be 3 answers and two "|" characters in the response. ' + \
                'for example if the question has answers of strings X, Y, Z, then the answer is formatted "X | Y | Z"'
        }
    ]

    responses = []

    for genre in genre_name_list:
        print('    generating responses for genre', genre)
        description_query = f'can you give me a short, 20 word or less description for the music genre {genre}?'
        origin_date_query = f'when was the music genre {genre} first created? give me either a year or a time period.'
        origin_location_query = f'where was the music genre {genre} formed? dont repeat the question in the answer.'
    
        messages.append({'role': 'user', 'content': description_query})
        messages.append({'role': 'user', 'content': origin_date_query})
        messages.append({'role': 'user', 'content': origin_location_query})

        data = requests.post(openai_api_base_url, headers={
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {openai_api_secret_key}'
        }, json={
            'model' : 'gpt-3.5-turbo-16k',
            'messages' : messages,
            'temperature' : 0.7
        }).json()

        responses.append({'genre': genre, 'data' : data})

        for i in range(3): 
            messages.pop(-1)

    genre_data = []

    for resp in responses:
        data_list = [i.strip() for i in resp['data']['choices'][0]['message']['content'].split('|')]

        genre_data.append({
            'name': resp['genre'],
            'description': data_list[0],
            'origin_date' : data_list[1],
            'origin_location' : data_list[2]
        })

    print('    success!')

    return genre_data

def _get_artists(authorization_token, playlist_data):
    print('getting artist data...')
    artists = set()

    for song in playlist_data:
        for artist_data in song['artists']:
            artist_id = artist_data['id']
            artists.add(artist_id)

    artists = list(artists)
    url = f'{spotify_api_base_url}/artists?ids={",".join(artists)}'
    headers = {
        'Authorization' : f'Bearer {authorization_token}'
    }
    artist_data = requests.get(url, headers=headers).json()['artists']

    for artist_dp in artist_data:
        print(f'    generating responses for artist {artist_dp["name"]}')
        
        data = requests.post(openai_api_base_url, headers={
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {openai_api_secret_key}'
        }, json={
            'model' : 'gpt-3.5-turbo-16k',
            'messages' : [
                {
                    'role': 'user',
                    'content': f'give me a short description about the singer/ artist {artist_dp["name"]}. give two short yet descriptive sentences!'
                }
            ],
            'temperature' : 0.7
        }).json()

        bio = data['choices'][0]['message']['content']
        artist_dp['bio'] = bio

    print('    success!')

    return artist_data

def _get_playlist(authorization_token, playlist_id = '37i9dQZEVXbLp5XoPON0wI', limit = 50):
    print('getting playlist data...')
    url = f'{spotify_api_base_url}/playlists/{playlist_id}/tracks?limit={limit}'

    headers = {
        'Authorization' : f'Bearer {authorization_token}'
    }

    resp = requests.get(url, headers=headers).json()['items']
    playlist_data = list(map(lambda x: x['track'], resp))

    for ind, i in enumerate(playlist_data):
        del i['album']['available_markets']
        del i['available_markets']
        i['rank'] = ind + 1

    print('    success!')
    
    return playlist_data

def get_data(playlist_id, artists=True, genres=True, playlist_limit = 50):
    authorization_token = _get_auth_token()
    playlist_data = _get_playlist(authorization_token, playlist_id, playlist_limit)

    artists_data = None
    if artists:
        artists_data = _get_artists(authorization_token, playlist_data)

    genre_data = None
    if genres:
        genre_names = set()
        for artist in artists_data:
            for genre in artist['genres']:
                genre_names.add(genre)
        genre_names = list(genre_names)
        genre_data = _get_genres(authorization_token, genre_names)

    return playlist_data, artists_data, genre_data

def main():
    # top50, artists, genres = get_data('37i9dQZEVXbLp5XoPON0wI')

    # pd.DataFrame(top50).to_csv('./data/top50.csv')
    # pd.DataFrame(artists).to_csv('./data/artists.csv')
    # pd.DataFrame(genres).to_csv('./data/genres.csv')

    print('dont run me!!!')

if __name__ == '__main__':
    main()