import unittest
from models import app, db, Song, Artist, Genre
import os 

class DBTestCases(unittest.TestCase):
  
  def setUp(self):
    app.config['TESTING'] = True
    database_uri = os.getenv('SQLALCHEMY_DATABASE_URI')
    app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
    db.create_all()

  def tearDown(self):
    db.session.remove()
    db.drop_all()
  
  def test_song_1(self):
    song = Song(rank = 51, name = 'Parking Lot', duration = '02:55')
    db.session.add(song)
    db.session.commit()
    
    row = db.session.query(Song).filter_by(rank = 51).one()
    self.assertEqual(row.rank, 51)
    self.assertEqual(row.name, 'Parking Lot')
    self.assertEqual(row.duration, '02:55')
    
    db.session.query(Song).filter_by(rank = 51).delete()
    db.session.commit()
  
  def test_song_2(self):
    artist = Artist(id = 30, name = 'Don Toliver', popularity = 39, followers = 12894)
    db.session.add(artist)
    db.session.commit()
    song = Song(rank = 51, name = 'After Party', duration = '02:48', artist = db.session.query(Artist).filter_by(name = 'Don Toliver').one())
    db.session.add(song)
    db.session.commit()
    
    song = db.session.query(Song).filter_by(rank = 51).one()
    self.assertEqual(song.artist_id, 30)
    
    db.session.query(Song).filter_by(rank = 51).delete()
    db.session.query(Artist).filter_by(id = 30).delete()
    db.session.commit()
    
  def test_song_3(self):
    genre = Genre(id = 30, name = 'unknown', origin_date = 'Late 1950s', origin_location = 'United States')
    db.session.add(genre)
    db.session.commit()
    song = Song(rank = 51, name = 'the boy is mine', duration = '02:54', genre = db.session.query(Genre).filter_by(name = 'unknown').one())
    db.session.add(song)
    db.session.commit()
    
    song = db.session.query(Song).filter_by(rank = 51).one()
    self.assertEqual(song.genre_id, 30)
    
    db.session.query(Song).filter_by(rank = 51).delete()
    db.session.query(Genre).filter_by(id = 30).delete()
    db.session.commit()
  
  def test_artist_1(self):
    artist = Artist(id = 30, name = 'Ariana Grande', popularity = 94, followers = 543879)
    db.session.add(artist)
    db.session.commit()
    
    row = db.session.query(Artist).filter_by(id = 30).one()
    self.assertEqual(row.id, 30)
    self.assertEqual(row.name, 'Ariana Grande')
    self.assertEqual(row.popularity, 94)
    self.assertEqual(row.followers, 543879)
    
    db.session.query(Artist).filter_by(id = 30).delete()
    db.session.commit()
    
  def test_artist_2(self):
    artist = Artist(id = 30, name = 'Coldplay', popularity = 72, followers = 32456)
    db.session.add(artist)
    db.session.commit()
    artist = db.session.query(Artist).filter_by(id = 30).one()
    song = Song(rank = 51, name = 'Hymn for the Weekend', duration = '04:19', artist = artist)
    db.session.add(song)
    song = Song(rank = 52, name = 'A Sky Full of Stars', duration = '04:29', artist = artist)
    db.session.add(song)
    db.session.commit()
    
    self.assertEqual(artist.songs[0].name, 'Hymn for the Weekend')
    self.assertEqual(artist.songs[1].name, 'A Sky Full of Stars')
    
    db.session.query(Song).filter_by(rank = 51).delete()
    db.session.query(Song).filter_by(rank = 52).delete()
    db.session.query(Artist).filter_by(id = 30).delete()
    db.session.commit()
    
  def test_artist_3(self):
    genre = Genre(id = 35, name = 'unknown', origin_date = '1970s', origin_location = 'Bronx, New York')
    db.session.add(genre)
    db.session.commit()
    artist = Artist(id = 30, name = 'Kendrick Lamar', popularity = 52, followers = 187352, 
        genre = db.session.query(Genre).filter_by(name = 'unknown').one())
    db.session.add(artist)
    db.session.commit()
    
    self.assertEqual(artist.genre_id, 35)
  
    db.session.query(Artist).filter_by(id = 30).delete()
    db.session.query(Genre).filter_by(id = 35).delete()
    db.session.commit()
  
  def test_genre_1(self):
    genre = Genre(id = 30, name = 'unknown', origin_date = '2010s', origin_location = 'Atlanta, Georgia')
    db.session.add(genre)
    db.session.commit()
    
    row = db.session.query(Genre).filter_by(id = 30).one()
    self.assertEqual(row.id, 30)
    self.assertEqual(row.name, 'unknown')
    self.assertEqual(row.origin_date, '2010s')
    self.assertEqual(row.origin_location, 'Atlanta, Georgia')
    
    db.session.query(Genre).filter_by(id = 30).delete()
    db.session.commit()
    
  def test_genre_2(self):
    genre = Genre(id = 30, name = 'unknown', origin_date = 'Late 1950s', origin_location = 'United States')
    db.session.add(genre)
    db.session.commit()
    genre = db.session.query(Genre).filter_by(id = 30).one()
    song = Song(rank = 51, name = 'Into You', duration = '04:04', genre = genre)
    db.session.add(song)
    song = Song(rank = 52, name = 'Thinking Bout You', duration = '03:20', genre = genre)
    db.session.add(song)
    db.session.commit()
    
    self.assertEqual(genre.songs[0].name, 'Into You')
    self.assertEqual(genre.songs[1].name, 'Thinking Bout You')
    
    db.session.query(Song).filter_by(rank = 51).delete()
    db.session.query(Song).filter_by(rank = 52).delete()
    db.session.query(Genre).filter_by(id = 30).delete()
    db.session.commit()
  
  def test_genre_3(self):
    genre = Genre(id = 30, name = 'unknown', origin_date = '1970s', origin_location = 'Bronx, New York')
    db.session.add(genre)
    db.session.commit()
    genre = db.session.query(Genre).filter_by(id = 30).one()
    artist = Artist(id = 34, name = 'Travis Scott', popularity = 1, followers = 734216, genre = genre)
    db.session.add(artist)
    artist = Artist(id = 36, name = 'Kanye West', popularity = 3, followers = 219890, genre = genre)
    db.session.add(artist)
    db.session.commit()
    
    self.assertEqual(genre.artists[0].name, 'Travis Scott')
    self.assertEqual(genre.artists[1].name, 'Kanye West')
    
    db.session.query(Artist).filter_by(id = 34).delete()
    db.session.query(Artist).filter_by(id = 36).delete()
    db.session.query(Genre).filter_by(id = 30).delete()
    db.session.commit()
  

if __name__ == '__main__':
  unittest.main()