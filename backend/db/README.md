# HitHub Database + PostgreSQL Setup in 5 Steps

This guide will help you set up a PostgreSQL database and connect it to your Flask application using SQLAlchemy.

## Prerequisites

- PostgreSQL (PSQL) installed
- PSQL database of name "hithub" is created
- The psql database URI is saved as an environment variable

## Step 1: Install PostgreSQL

### Windows / MacOS

1. Download PostgreSQL from [PostgreSQL Downloads](https://www.postgresql.org/download/).
2. Run the installer and follow the setup instructions.

## Step 2: Create the Database

### Using PostgreSQL Command Line

1. Open your terminal or command prompt.
2. Connect to the PostgreSQL server using the `psql` command-line tool:
    ```bash
    psql -U postgres
    ```
    Replace `postgres` with your PostgreSQL username.
3. Create the database:
    ```sql
    CREATE DATABASE hithub;
    ```

    Maybe you would like to create an entirely new user for this database:
    ```sql
    -- Create a new user role with a password
    CREATE USER myuser WITH PASSWORD 'mypassword';

    -- Grant privileges to the user on the database
    GRANT ALL PRIVILEGES ON DATABASE mydatabase TO myuser;
    ```
    replace the corresponding details in this template.

### Using pgAdmin

1. Open pgAdmin and connect to your PostgreSQL server.
2. Right-click on `Databases` in the tree view and select `Create > Database...`.
3. Enter a name for your database (e.g., `mydatabase`) and click `Save`.

## Step 3: Setting Database Connection URI and Environment Variable

### Database Connection URI Setup

To connect your Flask application to a PostgreSQL database using SQLAlchemy, you need to construct a Database Connection URI. This URI contains details such as the username, password, host, port, and database name.

#### Format of the Database Connection URI:

```plaintext
postgresql://username:password@host:port/hithub
```
Replace the sections `username`, `password`, `host` (which defaults to 5432), and `port` to their corresponding values.<br>
Note that your username can be found by running the following:
```plaintext
psql -U postgres
postgres=# \du
```

## Step 4: Setting Database Connection Environment Variable

To work with your newly created database, export this database connection URI to your environment variable of name `SQLALCHEMY_DATABASE_URI`. The Python script will search for this environment variable in order to connect to your database.<br>

### Setting Environment Variable in Bash:

```bash
export SQLALCHEMY_DATABASE_URI="<uri>"
```

### Setting Environment Variable in Command Prompt (Windows):

```cmd
set SQLALCHEMY_DATABASE_URI=<uri>
```


### Setting Environment Variable in Powershell (Windows):

```powershell
$env:SQLALCHEMY_DATABASE_URI = "<uri>"
```

notice the quotation mark requirement in this example.

Only after properly exporting this environment variable are you able to run the database related Python scripts to interact with the database.


## Step 5: Install Dependencies

Ensure that you have `Flask-SQLAlchemy` and `psycopg2` installed in your Python environment. You can install them using `pip`. There is a `requirements.txt` file in this project root directory. You may install dependencies from there: 

### Installation Example in Bash:

```bash
user@computer .../hithub
$ pip install -r requirements.txt
```