import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__, template_folder='../templates')
CORS(app)
app.app_context().push()
database_uri = os.getenv('SQLALCHEMY_DATABASE_URI', 'postgresql+psycopg2://postgres:BobLivesHouse2024@/hithub?host=/cloudsql/cs-373-hithub:us-central1:hithubinstance')
app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
app.config['SQLALCHEMY_TRACK MODIFICATIONS'] = True

db = SQLAlchemy(app)

class Song(db.Model):
  __tablename__ = 'song'

  rank = db.Column(db.Integer, primary_key = True)
  name = db.Column(db.String(50), nullable = False)
  duration = db.Column(db.String(5), nullable = False)
  image = db.Column(db.String(100), nullable = True)
  spotify_page = db.Column(db.String(100), nullable = True)
  artist_id = db.Column(db.Integer, db.ForeignKey('artist.id', onupdate = 'CASCADE'), nullable = True)
  genre_id = db.Column(db.Integer, db.ForeignKey('genre.id', onupdate = 'CASCADE'), nullable = True)
  
  def to_dict(self):
    return {
      'rank': self.rank,
      'name': self.name,
      'duration': self.duration,
      'image': self.image,
      'spotify_page': self.spotify_page,
      'artist_id': self.artist_id,
      'artist_name': db.session.query(Artist).filter_by(id = self.artist_id).one().name,
      'genre_id': self.genre_id,
      'genre_name': db.session.query(Genre).filter_by(id = self.genre_id).one().name
    }


class Artist(db.Model):
  __tablename__ = 'artist'
  
  id = db.Column(db.Integer, primary_key = True)
  name = db.Column(db.String(30), unique = True, nullable = False)
  popularity = db.Column(db.Integer, nullable = False)
  followers = db.Column(db.Integer, nullable = False)
  biography = db.Column(db.String(500), nullable = True)
  image = db.Column(db.String(100), nullable = True)
  spotify_page = db.Column(db.String(100), nullable = True)
  songs = db.relationship('Song', backref='artist', cascade = 'all, delete-orphan')
  genre_id = db.Column(db.Integer, db.ForeignKey('genre.id', onupdate = 'CASCADE'), nullable = True)
  
  def to_dict(self):
    return {
      'id': self.id,
      'name': self.name,
      'popularity': self.popularity,
      'followers': self.followers,
      'biography': self.biography,
      'image': self.image,
      'spotify_page': self.spotify_page,
      'genre_id': self.genre_id,
      'genre_name': db.session.query(Genre).filter_by(id = self.genre_id).one().name,
      'songs': [song.name for song in self.songs],
      'song_ids': [db.session.query(Song).filter_by(name = song.name).one().rank for song in self.songs]
    }
  

class Genre(db.Model):
  __tablename__ = 'genre'
  
  id = db.Column(db.Integer, primary_key = True)
  name = db.Column(db.String(30), unique = True, nullable = False)
  description = db.Column(db.String(300), unique = True, nullable = True)
  origin_date = db.Column(db.String(200), nullable = False)
  origin_location = db.Column(db.String(200), nullable = False)
  songs = db.relationship('Song', backref='genre', cascade = 'all, delete-orphan')
  artists = db.relationship('Artist', backref='genre', cascade = 'all, delete-orphan')
  video_id = db.Column(db.String(30), nullable = True)
  
  def to_dict(self):
    return {
      'id': self.id,
      'name': self.name,
      'description': self.description,
      'origin_date': self.origin_date,
      'origin_location': self.origin_location,
      'video_id': self.video_id,
      'songs': [song.name for song in self.songs],
      'song_ids': [db.session.query(Song).filter_by(name = song.name).one().rank for song in self.songs],
      'artists': [artist.name for artist in self.artists],
      'artist_ids': [db.session.query(Artist).filter_by(name = artist.name).one().id for artist in self.artists]
    }


db.drop_all()
db.create_all()