import os
import requests
from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response, abort
from flask_cors import CORS
from db.create_db import app, db, Song, Artist, Genre
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

song_info = [{'rank': 1, 'title': 'Please Please Please', 'artist': 'Sabrina Carpenter', 'genre': 'Pop', 'streams': '3240211', 'cover_photo': 'https://linkstorage.linkfire.com/medialinks/images/722315cc-42f2-450d-80e0-78fe03dad102/artwork-440x440.jpg'}, \
                {'rank': 2, 'title': 'Not Like Us', 'artist': 'Kendrick Lamar', 'genre': 'Hip-Hop', 'streams': '2750668', 'cover_photo': 'https://nomusica.com/wp-content/uploads/2024/05/Kendrick-Lamar-Not-Like-Us-Album-Cover.jpg'}, \
                {'rank': 3, 'title': 'Espresso', 'artist': 'Sabrina Carpenter', 'genre': 'Pop', 'streams': '2544330', 'cover_photo': 'https://images.genius.com/f8d1d714034d97ace5e4884bb320a60e.1000x1000x1.png'}, \
                {'rank': 4, 'title': 'I Had Some Help (Feat. Morgan Wallen)', 'artist': 'Post Malone', 'genre': 'Country', 'streams': '2354435', 'cover_photo': 'https://i1.sndcdn.com/artworks-2b1fe0Al1ovI-0-t500x500.jpg'}]

@app.route('/', methods=['GET','POST'])
def index():
    if request.method == 'POST':
        searchString = request.form['name'] 
        filterID = int(request.form['category']) 
        if filterID == 1:
            res = list(filter(lambda song: song['name'].lower().startswith(searchString.lower()), song_info))
            return render_template('index.html', song_info = res)
        elif filterID == 2:
            res = list(filter(lambda song: song['artist'].lower().startswith(searchString.lower()), song_info))
            return render_template('index.html', song_info = res)
        elif filterID == 3:
            res = list(filter(lambda song: song['genre'].lower().startswith(searchString.lower()), song_info))
            return render_template('index.html', song_info = res)
   
    return render_template('index.html', song_info = song_info)

# API endpoints
def _get_sort_args(primary_key):
      return request.args.get('sort_by', primary_key, type = str), request.args.get('sort_order', 'ascending', type = str)
      
@app.route('/api/GetAllSongs', methods = ['GET'])
def get_all_songs():
  sort_by, sort_order = _get_sort_args('rank')
  sort_attr = getattr(Song, sort_by) if hasattr(Song, sort_by) else Song.rank
  
  if sort_by == 'name':
      sort_attr = func.lower(sort_attr)
  
  order = sort_attr.desc() if sort_order == 'descending' else sort_attr.asc()
  
  songs = db.session.query(Song).order_by(order).paginate()
  
  return jsonify([song.to_dict() for song in songs])
  
      
@app.route('/api/GetSong', methods = ['GET'])
def get_song():
  song_id = request.args.get('song_id', 1, type = int)
  try:
    song = db.session.query(Song).filter_by(rank = song_id).one()
  except:
    abort(404)
  
  return jsonify(song.to_dict())

@app.route('/api/GetAllArtists', methods = ['GET'])
def get_all_artists():
  sort_by, sort_order = _get_sort_args('id')
  sort_attr = getattr(Artist, sort_by) if hasattr(Artist, sort_by) else Artist.id
  
  if sort_by == 'name':
      sort_attr = func.lower(sort_attr)
  
  order = sort_attr.desc() if sort_order == 'descending' else sort_attr.asc()
  
  artists = db.session.query(Artist).order_by(order).paginate()
  
  return jsonify([artist.to_dict() for artist in artists])

@app.route('/api/GetArtist', methods = ['GET'])
def get_artist():
  artist_id = request.args.get('artist_id', 1, type = int)
  try:
    artist = db.session.query(Artist).filter_by(id = artist_id).one()
  except:
    abort(404)
  
  return jsonify(artist.to_dict())

@app.route('/api/GetAllGenres', methods = ['GET'])
def get_all_genres():
  sort_by, sort_order = _get_sort_args('id')
  sort_attr = getattr(Genre, sort_by) if hasattr(Genre, sort_by) else Genre.id
  
  if sort_by == 'name':
      sort_attr = func.lower(sort_attr)
  
  order = sort_attr.desc() if sort_order == 'descending' else sort_attr.asc()
  
  genres = db.session.query(Genre).order_by(order).paginate()
  
  return jsonify([genre.to_dict() for genre in genres])

@app.route('/api/GetGenre', methods = ['GET'])
def get_genre():
  genre_id = request.args.get('genre_id', 1, type = int)
  try:
    genre = db.session.query(Genre).filter_by(id = genre_id).one()
  except:
    abort(404)
  
  return jsonify(genre.to_dict())

def _get_all_sort_key(e):
  return e['name'].lower()
  
@app.route('/api/GetAll', methods = ['GET'])
def get_all():
  sort_order = request.args.get('sort_order', 'ascending', type = str)
  song_order = getattr(Song, 'name').desc() if sort_order == 'descending' else getattr(Song, 'name').asc()
  artist_order = getattr(Artist, 'name').desc() if sort_order == 'descending' else getattr(Artist, 'name').asc()
  genre_order = getattr(Genre, 'name').desc() if sort_order == 'descending' else getattr(Genre, 'name').asc()
  
  songs = db.session.query(Song).order_by(song_order).all()
  artists = db.session.query(Artist).order_by(artist_order).all()
  genres = db.session.query(Genre).order_by(genre_order).all()
  all = [element.to_dict() for element in songs + artists + genres]
  
  if (sort_order == 'descending'):
    all.sort(reverse = True, key = _get_all_sort_key)
  else:
    all.sort(key = _get_all_sort_key)
  
  return jsonify(all)

@app.errorhandler(404)
def response_error(e):
    return jsonify(error='Resource not found'), 404

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)