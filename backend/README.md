# How to Run the Flask API

This Flask API is organized as a Python packaged module. In order to run this API code, you must actually run this project form the current directory like so: 
```bash
python -m api.main
```
The `-m` argument indicates to the Python interpreter that the project is to be run in module-mode.

## Prerequisites
1. There should be a Postgresql database already prepared. Follow the `README.md` directions in the `backend/db` folder for more instructions on that. 
2. The database connection URI environment variable, `SQLALCHEMY_DATABASE_URI`, should be set before running the module. The instructions for this are also located in the `README.md` file under `backend/db`.