#!/bin/bash

SQLALCHEMY_SERVER_URI="postgresql://hithubdev:password123@ec2-13-58-46-174.us-east-2.compute.amazonaws.com:5432"
export SQLALCHEMY_DATABASE_URI="${SQLALCHEMY_SERVER_URI}/hithub"

# first ensure that the nginx reverse-proxy is set
sudo systemctl reload nginx
sudo systemctl start nginx
sleep 5
echo "nginx is Running!"

# git pull + update repository settings
git pull origin main

# update the database configuration
cd backend
source ./venv/bin/activate
pip install -r requirements.txt
python3 -m db.create_db

# run the flask application
if pgrep -f "main.py" >/dev/null; then
    echo "Flask API is already running. Restarting..."
    pkill -f "main.py"
    sleep 1
else
    echo "Flask API is not running. Starting..."
fi

nohup venv/bin/python3 main.py > ../../logging/api.log 2>&1 &

cd ~/hithub

echo "Script execution completed."