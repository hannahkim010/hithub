import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { deployed_url, local_url } from "../components/axios_url"; 
import spotifyLogo from "../components/spotifyLogo.png"

function OneArtist() {
  const { id } = useParams();
  const [artistData, setArtistData] = useState({
    name: "",
    image: "",
    biography: "",
    followers: "",
    genre_name: "",
    genre_id: 0,
    songs: [""],
    song_ids: [0],
    popularity: "",
    spotify_page: ""
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const artist = (await local_url.get(`/api/GetArtist?artist_id=${Number(id)}`)).data;
        setArtistData(artist);
        console.log(artistData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [id]);

  const { name, image, biography, followers, genre_name, genre_id, songs, song_ids, popularity, spotify_page } = artistData

  return (
    <div className="container-one-thing">
      <div className="div-left">
        <img src={image} alt-text="artist picture"></img>
      </div>

      <div className="div-right">
        <h1><b>{name}</b></h1>
        <h3><b> - Biography: </b></h3>
        <p>{biography}</p>
        <h4><b> - Genre: </b>{" "}<Link to={`/oneGenre/${genre_id}`} style={{ color: 'black' }}>{genre_name}</Link></h4>
        <h4><b> - Popularity Score: </b>{popularity}</h4>
        <h4><b> - Followers: </b>{followers}</h4>
        <h4><b> - Songs: </b>{" "} 
        {songs.map((song, index) => (
        <React.Fragment key={index}>
            <Link to={`/oneSong/${song_ids[index]}`} style={{ color: 'black' }}>
                {song}
            </Link>
            {index !== songs.length - 1 && ', '}
        </React.Fragment>
          ))
        }
        </h4> <br/>
        <div className="spotify-logo-container-75">
          <Link to={spotify_page}>
            <img src={spotifyLogo} alt="Spotify" style={{ width: '50px', height: '50px' }} />
          </Link>
        </div>
      </div>
      
    </div>
  );
}
export default OneArtist;
