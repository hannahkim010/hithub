import React, { useState, useEffect } from "react";
import GenreCard from "../components/GenreCard";
import Search from "../components/search";
import PaginationBar from "../components/PaginationBar";
import { deployed_url, local_url } from "../components/axios_url"; 
import SearchDropdown from "../components/SearchDropdown";

function checkQueryInCard(card: any, query: string, filterType: string) {
  const ignoreKeys: string[] = ['song_ids', 'artist_ids'];
  if (filterType === 'Songs') {
    if (card.back.songs.some((item: any) => item.toLowerCase().includes(query.toLowerCase()))) {
      return true;
    }
  }
  else if (filterType === 'Artists') {
    if (card.back.artists.some((item: any) => item.toLowerCase().includes(query.toLowerCase()))) {
      return true;
    }
  }
  else if (filterType === 'Genres') {
    if (card.back.name.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  else {
    for (let key in card.back) {
      if (!ignoreKeys.includes(key)) {
        const value = card.back[key];
        if (typeof value === 'string' && value.toLowerCase().includes(query.toLowerCase())) {
          return true;
        } else if (typeof value === 'number' && value.toString().includes(query)) {
          return true;
        } else if (Array.isArray(value)) {
          if (value.some((item: any) => typeof item === 'string' && item.toLowerCase().includes(query.toLowerCase()))) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

function Genres() {
  const [data, setData] = useState<Card[]>([]);
  const [newData, setNewData] = useState<Card[]>([]);
  const [query, setQuery] = useState<string>("");
  const [pageIndex, setPageIndex] = useState<number>(0)
  const [showMessage, setShowMessage] = useState<boolean>(true); 
  const numPerPage: number = 9;
  const [filterType, setFilterType] = useState<string>("Filter");
  const [sortType, setSortType] = useState<string>("Sort By");
  const [sortOrder, setSortOrder] = useState<string>("Sort Order");
  const searchItems = ['All', 'Songs', 'Artists', 'Genres'];
  const sortItems = ['Name','Popularity']
  const orderItems = ['Ascending', 'Descending']

  const changePage = (newIndex: number) => {
    setPageIndex(newIndex);
  }

  const handleSearch = (newQuery: string) => {
    console.log("Searching for:", newQuery);
    setQuery(newQuery);
  };

  const handleFilter = (newFilterType: string) => {
    setFilterType(newFilterType);
  }

  const handleSortType = (newSortType: string) => {
    setSortType(newSortType);
  }

  const handleSortOrder = (newSortOrder: string) => {
    setSortOrder(newSortOrder);
  }

  interface Card {
    id: string;
    variant: "hover" | "click";
    front: {
      name: string;
    };
    back: {
      name: string;
      origin_date: string;
      origin_location: string;
      songs: string[];
      artists: string[];
      artist_ids: number[];
      song_ids: number[];
    };
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await local_url.get("/api/GetAllGenres?per_page=50");

        const genresData: Card[] = [];
        // Limiting to the first 3 songs for demonstration purposes
        for (let i = 0; i < response.data.length; i++) {
          const genre = response.data[i];

          const newCard: Card = {
            id: genre.id.toString(),
            variant: "click",
            front: {
              name: genre.name,
            },
            back: {
              name: genre.name,
              origin_date: genre.origin_date,
              origin_location: genre.origin_location,
              songs: genre.songs,
              artists: genre.artists,
              artist_ids: genre.artist_ids,
              song_ids: genre.song_ids,
            },
          };

          genresData.push(newCard);
        }

        setData(genresData);
        setNewData(genresData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    let filteredArray = [...data];

    if (query !== '') {
      filteredArray = data.filter(card => checkQueryInCard(card, query, filterType));
    }

    // Sort filteredArray based on sortOrder and name field
    const sortedArray = filteredArray.sort((a, b) => {
      if(sortType === "Name"){
        let nameA = a.back.name.toUpperCase();
        let nameB = b.back.name.toUpperCase();
        if (sortOrder === 'Descending') {
          return nameB.localeCompare(nameA); 
        } else {
          return nameA.localeCompare(nameB);
        }
      } else{
        let popularityA = a.back.songs.length;
        let popularityB= b.back.songs.length;
        if (sortOrder === 'Descending') {
          return popularityA - popularityB;
        } else {
          return popularityB - popularityA;
        }
      }
    });
    setNewData(sortedArray);
  }, [query, sortOrder, filterType, sortType, data]);

  const closeMessage = () => {
    setShowMessage(false);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowMessage(false);
    }, 2200); 

    return () => clearTimeout(timer);
  }, []);

  return (
    <div className="container">
      {showMessage && (
        <div className="message-overlay" onClick={closeMessage}>
          <div className="message-content">
            <p className="message-text">Click to flip cards!</p>
          </div>
        </div>
      )}
      <div data-bs-theme="glass" className="sort-filter-container search-row">
        <Search onSearch={handleSearch} />
        <SearchDropdown searchType={filterType} handleSwitch={handleFilter} items={searchItems}/>
        <SearchDropdown searchType={sortType} handleSwitch={handleSortType} items={sortItems}/>
        <SearchDropdown searchType={sortOrder} handleSwitch={handleSortOrder} items={orderItems}/>
      </div>
      <div className="row">
        {
            newData.slice(pageIndex * numPerPage, pageIndex * numPerPage + numPerPage).map((card, index) => (
                <div className="col col-custom-sm col-custom-md col-custom-lg">
                <GenreCard
                key={card.id}
                id={card.id}
                variant={card.variant}
                front={card.front}
                back={card.back}
                query={query}
                filterType={filterType}
              />
              </div>
            ))
        }
      </div>
      <PaginationBar pageIndex={pageIndex} changePage={changePage} dataLength={newData.length} numPerPage={numPerPage}/>
    </div>
  );
}

export default Genres;
