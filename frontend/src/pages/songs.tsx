import React, { useState, useEffect } from "react";
import SongCard from "../components/SongCard";
import Search from "../components/search";
import PaginationBar from "../components/PaginationBar";
import "../styles/Songs.css";
import { deployed_url, local_url } from "../components/axios_url"; 
import SearchDropdown from "../components/SearchDropdown";

function checkQueryInCard(card: any, query: string, filterType: string) {
  const ignoreKeys: string[] = ['artist_id', 'genre_id', 'song_link'];
  if (filterType === 'Songs') {
    if (card.back.name.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  else if (filterType === 'Artists') {
    if (card.back.artist.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  else if (filterType === 'Genres') {
    if (card.back.genre.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  else {
    for (let key in card.back) {
      if (!ignoreKeys.includes(key)) {
        const value = card.back[key];
        if (typeof value === 'string' && value.toLowerCase().includes(query.toLowerCase())) {
          return true;
        } else if (typeof value === 'number' && value.toString().includes(query)) {
          return true;
        }
      }
    }
  }

  return false;
}

function Songs() {
  const [data, setData] = useState<Card[]>([]);
  const [newData, setNewData] = useState<Card[]>([]);
  const [query, setQuery] = useState<string>("");
  const [pageIndex, setPageIndex] = useState<number>(0);
  const [showMessage, setShowMessage] = useState<boolean>(true); 
  const numPerPage: number = 9;
  const [filterType, setFilterType] = useState<string>("Filter");
  const [sortType, setSortType] = useState<string>("Sort By");
  const [sortOrder, setSortOrder] = useState<string>("Sort Order");
  const searchItems = ['All', 'Songs', 'Artists', 'Genres'];
  const sortItems = ['Name','Popularity']
  const orderItems = ['Ascending', 'Descending']

  const changePage = (newIndex: number) => {
    setPageIndex(newIndex);
  };

  const handleSearch = (newQuery: string) => {
    console.log("Searching for:", newQuery);
    setQuery(newQuery);
  };

  const handleFilter = (newFilterType: string) => {
    setFilterType(newFilterType);
  }

  const handleSortType = (newSortType: string) => {
    setSortType(newSortType);
  }

  const handleSortOrder = (newSortOrder: string) => {
    setSortOrder(newSortOrder);
  }

  useEffect(() => {
    let filteredArray = [...data];

    if (query !== '') {
      filteredArray = data.filter(card => checkQueryInCard(card, query, filterType));
    }

    let sortedArray = [];
    if (sortType === 'Name') {
      sortedArray = filteredArray.sort((a, b) => {
        const nameA = a.back.name.toUpperCase();
        const nameB = b.back.name.toUpperCase();
        
        if (sortOrder === 'Descending') {
          return nameB.localeCompare(nameA);
        } else {
          return nameA.localeCompare(nameB);
        }
      });
    }
    else {
      sortedArray = filteredArray.sort((a, b) => {
        if (sortOrder === 'Descending') {
          return b.back.rank - a.back.rank;
        } else {
          return a.back.rank - b.back.rank;
        }
      });
    }

    setNewData(sortedArray);
    
  }, [query, sortOrder, sortType, filterType, data]);

  interface Card {
    id: string;
    variant: "hover" | "click";
    front: {
      rank: number;
      name: string;
      image: string;
    };
    back: {
      rank: number;
      name: string;
      artist: string;
      genre: string;
      duration: string;
      artist_id: number;
      genre_id: number;
      song_link: string;
    };
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await local_url.get("/api/GetAllSongs?per_page=50");

        const songsData: Card[] = [];
        for (let i = 0; i < response.data.length; i++) {
          const song = response.data[i];

          const newCard: Card = {
            id: song.rank.toString(),
            variant: "click",
            front: {
              rank: song.rank,
              name: song.name,
              image: song.image,
            },
            back: {
              rank: song.rank,
              name: song.name,
              artist: song.artist_name,
              genre: song.genre_name,
              duration: song.duration,
              artist_id: song.artist_id,
              genre_id: song.genre_id,
              song_link: song.spotify_page
            },
          };

          songsData.push(newCard);
        }

        setData(songsData);
        setNewData(songsData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []); 

  useEffect(() => {
    let filteredArray = [...data];

    if (query !== '') {
      filteredArray = data.filter(card => checkQueryInCard(card, query, filterType));
    }

    // Sort filteredArray based on sortOrder and name field
    const sortedArray = filteredArray.sort((a, b) => {
      if(sortType === "Name"){
        let nameA = a.back.name.toUpperCase();
        let nameB = b.back.name.toUpperCase();
        if (sortOrder === 'Descending') {
          return nameB.localeCompare(nameA);
        } else {
          return nameA.localeCompare(nameB);
        }
      } else{
        let popularityA = a.back.rank;
        let popularityB= b.back.rank;
        if (sortOrder === 'Descending') {
          return popularityB - popularityA;
        } else {
          return popularityA - popularityB;
        }
      }
    });

    setNewData(sortedArray);
  }, [query, sortOrder, filterType, sortType, data]);

  const closeMessage = () => {
    setShowMessage(false);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowMessage(false);
    }, 2200); 

    return () => clearTimeout(timer);
  }, []);

  return (
    <div className="container">
      {showMessage && (
        <div className="message-overlay" onClick={closeMessage}>
          <div className="message-content">
            <p className="message-text">Click to flip cards!</p>
          </div>
        </div>
      )}

      <div data-bs-theme="glass" className="sort-filter-container search-row">
        <Search onSearch={handleSearch} />
        <SearchDropdown searchType={filterType} handleSwitch={handleFilter} items={searchItems}/>
        <SearchDropdown searchType={sortType} handleSwitch={handleSortType} items={sortItems}/>
        <SearchDropdown searchType={sortOrder} handleSwitch={handleSortOrder} items={orderItems}/>
      </div>
      <div className="row">
        {
            newData.slice(pageIndex * numPerPage, pageIndex * numPerPage + numPerPage).map((card, index) => (
                <div className="col col-custom-sm col-custom-md col-custom-lg">
                    <SongCard
                    key={card.id}
                    id={card.id}
                    variant={card.variant}
                    front={card.front}
                    back={card.back}
                    query={query}
                    filterType={filterType}
                    />
              </div>
            ))
        }
      </div>
      <PaginationBar pageIndex={pageIndex} changePage={changePage} dataLength={newData.length} numPerPage={numPerPage} />
    </div>
  );
}

export default Songs;


