import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import "../styles/oneSong.css";
import YouTubeVideo from "../components/YouTubeVideo";
import { deployed_url, local_url } from "../components/axios_url"; 

function OneGenre() {
  const { id } = useParams();
  const [genreData, setGenreData] = useState({
    name: "",
    origin_date: "",
    origin_location: "",
    artists: [],
    songs: [],
    artist_ids: [],
    song_ids: [],
    video_id: "",
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const genre = (await local_url.get(`/api/GetGenre?genre_id=${Number(id)}`)).data;
        setGenreData(genre);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [id]);

  const { name, origin_date, origin_location, artists, songs, artist_ids, song_ids, video_id } = genreData;

  return (
    <div className="container-one-thing">
      <div className="div-left">
        <div className="youtube">
          <YouTubeVideo videoID={video_id} />
        </div>
      </div>
      <div className="div-right">
        <h1><b>{name}</b></h1>
        <h3><b> - Origin Date: </b>{origin_date}</h3>
        <h3><b> - Origin Location: </b>{origin_location}</h3>
        <h3><b> - Songs: </b>
          {songs.map((song, index) => (
            <React.Fragment key={index}>
              <Link to={`/oneSong/${song_ids[index]}`} style={{ color: 'black' }}>
                {song}
              </Link>
              {index !== songs.length - 1 && ', '}
            </React.Fragment>
          ))}
        </h3>
        <h3><b> - Artists: </b>
          {artists.map((artist, index) => (
            <React.Fragment key={index}>
              <Link to={`/oneArtist/${artist_ids[index]}`} style={{ color: 'black' }}>
                {artist}
              </Link>
              {index !== artists.length - 1 && ', '}
            </React.Fragment>
          ))}
        </h3>
      </div>
    </div>
  );
}

export default OneGenre;
