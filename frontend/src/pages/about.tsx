import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../styles/about.css';

interface Member {
  name: string;
  role: string;
  bio: string;
  photo: string;
  issuesCount: number;
  commitsCount: number;
  unitTest: number;
}

const About: React.FC = () => {
  const initialMembers: Member[] = [
        {
          name: 'Eduardo Mercado',
          role: 'Front-End',
          bio:
            'I am a senior CS major. In my free time, I enjoy playing soccer, pickleball, and video games.',
          photo: 'https://www.cs.utexas.edu/~eddym/images/pfp.jpg',
          issuesCount: 0,
          commitsCount: 0,
          unitTest: 0,
        },
        {
          name: 'Hannah Kim',
          role: 'Front-End',
          bio:
            'I am an incoming Senior CS major at UT Austin! In my free time, I enjoy cooking and binge watching movies.',
          photo: 'https://www.cs.utexas.edu/~hkim/headshot.JPEG',
          issuesCount: 0,
          commitsCount: 0,
          unitTest: 0,
        },
        {
          name: 'Abhinav Bichal',
          role: 'Back-End',
          bio:
            'I am a senior CS student at UT at Austin! In my free time, I enjoy playing basketball and pickleball.',
          photo: 'https://www.cs.utexas.edu/~abhinavb/headshot.JPG',
          issuesCount: 0,
          commitsCount: 0,
          unitTest: 40,
        },
        {
          name: 'Daniel Lim',
          role: 'Front-End',
          bio:
            'I am a Senior at UT Austin, double majoring in CS and Music. In my free time, I enjoy writing and board games.',
          photo: 'https://www.cs.utexas.edu/~dwl667/MeProfile.jpg',
          issuesCount: 0,
          commitsCount: 0,
          unitTest: 0,
        },
        {
          name: 'Vivian Nguyen',
          role: 'Front-End',
          bio:
            'I am a junior CS major at UT Austin. In my free time, I enjoy dancing and playing video games.',
          photo: 'https://www.cs.utexas.edu/~vivianqt/headshot.jpeg',
          issuesCount: 0,
          commitsCount: 0,
          unitTest: 0,
        },
        {
          name: 'Wajih Ahmed',
          role: 'Back-End',
          bio:
            'I am a senior CS major at UT Austin. In my free time, I enjoy playing video games and working out.',
          photo:
            'https://media.licdn.com/dms/image/D5603AQFoJHkCxvnQyA/profile-displayphoto-shrink_800_800/0/1696704156194?e=1725494400&v=beta&t=_PFLbZ1Wgxj09A-BEXP6vhPvt2gSgcVXlUCBBUk5hEM',
          issuesCount: 0,
          commitsCount: 0,
          unitTest: 39,
        },
  ];
  const resources = [
          {
            name: "AWS",
            url:
              "https://i.pinimg.com/736x/4a/41/7d/4a417d1f8cab870d4e93498ae1ae2d21.jpg",
          },
          {
            name: "Bootstrap",
            url:
              "https://www.stickerpress.in/media/products/800x800/df70d11c185d4e81aad3fc5f5b5b2576.jpg",
          },
          {
            name: "Flask",
            url:
              "https://www.stickerpress.in/media/products/800x800/83a78dbd45804be5bbd0dd2a82c50fb1.jpg",
          },
          {
            name: "GitLab",
            url:
              "https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/144_Gitlab_logo_logos-512.png",
          },
          {
            name: "Postman",
            url: "https://cdn.worldvectorlogo.com/logos/postman.svg",
          },
          {
            name: "VSCode",
            url:
              "https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/visual-studio-code-icon.png",
          },
          {
            name: "React",
            url: "https://www.svgrepo.com/show/303500/react-1-logo.svg",
          },
          {
            name: "PostgreSQL",
            url:
              "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1985px-Postgresql_elephant.svg.png",
          },
          {
            name: "SQLAlchemy",
            url:
              "https://cdn.hackersandslackers.com/2020/08/sqlalchemy2.png",
          },
          {
            name: "Axios",
            url:
              "https://logowik.com/content/uploads/images/axios5736.logowik.com.webp",
          },
          {
            name: "Discord",
            url: "https://cdn.prod.website-files.com/6257adef93867e50d84d30e2/636e0a6a49cf127bf92de1e2_icon_clyde_blurple_RGB.png",
          },
          {
            name: "Google Cloud Platform",
            url: "https://static-00.iconduck.com/assets.00/google-cloud-icon-2048x1646-7admxejz.png",
          }
  ];

  const [members, setMembers] = useState<Member[]>(initialMembers);
  const [totalIssues, setTotalIssues] = useState<number>(0);
  const [totalCommits, setTotalCommits] = useState<number>(0);
  const gitlabBaseUrl = 'https://gitlab.com/api/v4/projects/59247234/';
  const gitlabToken = 'glpat-_fxx9jq_xBDbwAs6xH6q';
  const user_list = ['eddymercado', 'hannahkim010', 'abhinavbichal', 'limdan8vq', 'vivianthun', 'Wajih5023'];
  const author_name_list = ['Eddy Mercado', 'Hannah Kim', 'thabat12', 'Daniel Lim', 'Vivian Nguyen', 'Wajih Ahmed'];
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchCommitsAndIssues = async () => {
      try {
        let commitsData: any[] = [];
        let page = 1;

        while (true) {
          const commitsResponse = await axios.get(`${gitlabBaseUrl}repository/commits`, {
            headers: { 'Authorization': `Bearer ${gitlabToken}` },
            params: { 'per_page': 100, 'page': page }
          });

          if (commitsResponse.data.length === 0) {
            break; // Exit loop if no more commits are returned
          }

          commitsData = commitsData.concat(commitsResponse.data);
          page++;
        }

        setTotalCommits(commitsData.length);

        const issuesRequests = user_list.map((user) => axios.get(`${gitlabBaseUrl}issues_statistics?author_username=${user}`, {
          headers: { 'Authorization': `Bearer ${gitlabToken}` }
        }));

        const issuesResponses = await Promise.all(issuesRequests);
        const updatedMembers = initialMembers.map((member, index) => {
          const issuesCount = issuesResponses[index].data.statistics.counts.closed;
          const commitsCount = commitsData.filter((commit: any) =>
            commit.author_name.startsWith(user_list[index]) || commit.author_name.startsWith(author_name_list[index])
          ).length;
          return { ...member, issuesCount, commitsCount };
        });

        setMembers(updatedMembers);

        const totalIssuesResponse = await axios.get(`${gitlabBaseUrl}issues_statistics`, {
          headers: { 'Authorization': `Bearer ${gitlabToken}` }
        });
        setTotalIssues(totalIssuesResponse.data.statistics.counts.closed);
        setError(null);
      } catch (err: any) {
        setError(err.message);
      } finally {
        setLoading(false);
      }
    };

    // check

    fetchCommitsAndIssues();
  }, []);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;

  return (
    <div className="container">
      <div className="blur-box">
        <h1><b>Our Goal</b></h1><br/>
        <p className="text-center p-info">
          Our goal is to keep music enthusiasts up to date with the current top
          hits in the United States and provide background on them.
        </p>
      <br />
      </div>
      <div className="blur-box">
      <h1><b>Our Team</b></h1><br/>
      <div className="row member-container">
        {members.map((member, index) => (
          <div className="col" key={index}>
            <div className="card border-secondary mb-3" style={{ width: '20rem' }}>
              <img className="card-img-top" src={member.photo} alt={member.name} />
              <div className="card-body">
                <h5 className="card-title">{member.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{member.role}</h6>
                <p className="card-text">{member.bio}</p>
                <h6 className="card-subtitle mb-2 text-muted">Issues Count: {member.issuesCount}</h6>
                <h6 className="card-subtitle mb-2 text-muted">Commits Count: {member.commitsCount}</h6>
                <h6 className="card-subtitle mb-2 text-muted">Unit Tests Count: {member.unitTest}</h6>
              </div>
            </div>
          </div>
        ))}
      </div>
      </div>

      <div className="blur-box">
        <h1 className="centered1"><b>GitLab Specs</b></h1><br/>
        < p className="p-info"></p>
        <p className="p-info">Total Issues: {totalIssues}</p>
        <p className="p-info">Total Commits: {totalCommits}</p>
        <p className="p-info">Total Unit Tests: 79</p>
        <br/>
      </div>
      
      <div className="blur-box">
        <h1 className="centered1"><b>Tools Used</b></h1><br/>
        <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3">
          {resources.map((resource, index) => (
            <div className="col mb-4" key={index}>
              <div className="card h-100">
                <img src={resource.url} className="card-img-top" alt={`Resource ${index}`} />
                <div className="tool-card-body d-flex flex-column justify-content-between">
                  <p className="tool-card-title text-center mb-0">{resource.name}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
  
      <div className="blur-box">
        <h1 className="centered1"><b>Work Links</b></h1><br/>
        <div className="gitlab-link p-info">
          <p><a href="https://gitlab.com/hannahkim010/hithub" style={{ color: 'black' }}>GitLab Repository</a></p>
        </div>
        <div className="gitlab-issue-link p-info">
          <p><a href="https://gitlab.com/hannahkim010/hithub/-/issues" style={{ color: 'black' }}>GitLab Issue Tracker</a></p>
        </div>
        <div className="postman-link p-info">
          <p><a href="https://documenter.getpostman.com/view/36578566/2sA3dvjsN7" style={{ color: 'black' }}>Postman</a></p>
        </div>
        <div className="gitlab-wiki-link p-info">
          <p><a href="https://gitlab.com/hannahkim010/hithub/-/wikis/Technical-Report?redirected_from=home" style={{ color: 'black' }}>GitLab Technical Report</a></p>
        </div><br/>
      </div> <br/>
    </div>
  );
};

export default About;
