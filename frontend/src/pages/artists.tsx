import ReactCardFlip from "react-card-flip";
import Search from "../components/search";
import React, { useState, useEffect } from "react";
import ArtistCard from "../components/ArtistCard";
import PaginationBar from "../components/PaginationBar";
import { deployed_url, local_url } from "../components/axios_url"; 
import SearchDropdown from "../components/SearchDropdown";

function checkQueryInCard(card: any, query: string, filterType: string) {
  const ignoreKeys: string[] = ['song_ids', 'genre_id', 'artist_link'];
  if (filterType === 'Songs') {
    if (card.back.songs.some((item: any) => item.toLowerCase().includes(query.toLowerCase()))) {
      return true;
    }
  }
  else if (filterType === 'Artists') {
    if (card.back.name.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  else if (filterType === 'Genres') {
    if (card.back.genre.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  else {
    for (let key in card.back) {
      if (!ignoreKeys.includes(key)) {
        const value = card.back[key];
        if (typeof value === 'string' && value.toLowerCase().includes(query.toLowerCase())) {
          return true;
        } else if (typeof value === 'number' && value.toString().includes(query)) {
          return true;
        } else if (Array.isArray(value)) {
          if (value.some((item: any) => typeof item === 'string' && item.toLowerCase().includes(query.toLowerCase()))) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

function Artists() {
  const [data, setData] = useState<Card[]>([]);
  const [newData, setNewData] = useState<Card[]>([]);
  const [query, setQuery] = useState<string>("");
  const [pageIndex, setPageIndex] = useState<number>(0)
  const [showMessage, setShowMessage] = useState<boolean>(true); 
  const numPerPage: number = 9;
  const [filterType, setFilterType] = useState<string>("Filter");
  const [sortType, setSortType] = useState<string>("Sort By");
  const [sortOrder, setSortOrder] = useState<string>("Sort Order");
  const searchItems = ['All', 'Songs', 'Artists', 'Genres'];
  const sortItems = ['Name','Popularity']
  const orderItems = ['Ascending', 'Descending']

  const changePage = (newIndex: number) => {
    setPageIndex(newIndex);
  }

  const handleSearch = (newQuery: string) => {
    console.log("Searching for:", newQuery);
    setQuery(newQuery);
  };

  const handleFilter = (newFilterType: string) => {
    setFilterType(newFilterType);
  }

  const handleSortType = (newSortType: string) => {
    setSortType(newSortType);
  }

  const handleSortOrder = (newSortOrder: string) => {
    setSortOrder(newSortOrder);
  }

  useEffect(() => {
    let filteredArray = [...data];

    if (query !== '') {
      filteredArray = data.filter(card => checkQueryInCard(card, query, filterType));
    }

    let sortedArray = [];
    if (sortType === 'Name') {
      sortedArray = filteredArray.sort((a, b) => {
        const nameA = a.back.name.toUpperCase();
        const nameB = b.back.name.toUpperCase();
        
        if (sortOrder === 'Descending') {
          return nameB.localeCompare(nameA);
        } else {
          return nameA.localeCompare(nameB);
        }
      });
    }
    else {
      sortedArray = filteredArray.sort((a, b) => {
        if (sortOrder === 'Descending') {
          return Number(b.id) - Number(a.id);
        } else {
          return Number(a.id) - Number(b.id);
        }
      });
    }

    setNewData(sortedArray);
    
  }, [query, sortOrder, sortType, filterType, data]);

  interface Card {
    id: string;
    variant: "hover" | "click";
    front: {
      name: string;
      image: string;
    };
    back: {
      name: string;
      bio: string;
      followers: number;
      genre: string;
      genre_id: number;
      popularity: number;
      songs: string[];
      song_ids: number[];
      artist_link: string;
    };
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await local_url.get("/api/GetAllArtists?per_page=50");

        const artistsData: Card[] = [];
        // Limiting to the first 3 songs for demonstration purposes
        for (let i = 0; i < response.data.length; i++) {
          const artist = response.data[i];

          const newCard: Card = {
            id: artist.id.toString(),
            variant: "click",
            front: {
              name: artist.name,
              image: artist.image,
            },
            back: {
              name: artist.name,
              bio: artist.biography,
              followers: artist.followers,
              genre: artist.genre_name,
              genre_id: artist.genre_id,
              songs: artist.songs,
              song_ids: artist.song_ids,
              popularity: artist.popularity,
              artist_link: artist.spotify_page
            }
          };

          artistsData.push(newCard);
        }

        setData(artistsData);
        setNewData(artistsData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []); // Empty dependency array ensures useEffect runs only once

  useEffect(() => {
    let filteredArray = [...data];

    if (query !== '') {
      filteredArray = data.filter(card => checkQueryInCard(card, query, filterType));
    }

    // Sort filteredArray based on sortOrder and name field
    const sortedArray = filteredArray.sort((a, b) => {
      if(sortType === "Name"){
        let nameA = a.back.name.toUpperCase();
        let nameB = b.back.name.toUpperCase();
        //by checking for descending, we can default to ascending while keeping the drop down name/type
        if (sortOrder === 'Descending') {
          return nameB.localeCompare(nameA);
        } else {
          return nameA.localeCompare(nameB);
        }
      } else{
        let popularityA = a.back.popularity;
        let popularityB= b.back.popularity;
        if (sortOrder === 'Descending') {
          return popularityA - popularityB; 
        } else {
          return popularityB - popularityA; 
        }
      }
    });

    setNewData(sortedArray);
  }, [query, sortOrder, filterType, sortType, data]);

  const closeMessage = () => {
    setShowMessage(false);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowMessage(false);
    }, 2200); 

    return () => clearTimeout(timer);
  }, []);

  return (
    <div className="container">
        {showMessage && (
        <div className="message-overlay" onClick={closeMessage}>
            <div className="message-content">
            <p className="message-text">Click to flip cards!</p>
            </div>
        </div>
        )}
        <div data-bs-theme="glass" className="sort-filter-container search-row">
        <Search onSearch={handleSearch} />
        <SearchDropdown searchType={filterType} handleSwitch={handleFilter} items={searchItems}/>
        <SearchDropdown searchType={sortType} handleSwitch={handleSortType} items={sortItems}/>
        <SearchDropdown searchType={sortOrder} handleSwitch={handleSortOrder} items={orderItems}/>
      </div>
      <div className="row">
        {
            newData.slice(pageIndex * numPerPage, pageIndex * numPerPage + numPerPage).map((card, index) => (
                <div className="col col-custom-sm col-custom-md col-custom-lg">
                    <ArtistCard
                    key={card.id}
                    id={card.id}
                    variant={card.variant}
                    front={card.front}
                    back={card.back}
                    query={query}
                    filterType={filterType}
                    />
              </div>
            ))
        }
      </div>
      <PaginationBar pageIndex={pageIndex} changePage={changePage} dataLength={newData.length} numPerPage={numPerPage}/>
    </div>
  );
}

export default Artists;
