import React, { useEffect } from "react";
// import navbar from "../components/navbar";
// import Search from "../components/search";
import { useState } from "react";
import "../styles/SplashPage.css";

import axios from "axios";

function SplashPage() {

  return (
    <div>
      <head>
        <style></style>
      </head>
      <div className="containerSplash">
        <img className="backgroundSplash"
          src="https://i.ibb.co/CQpzQ6Z/Album-Collage-Song.png"
          alt="collage">
        </img>
        <div className="center">
          {/* <h3 id="Welcome">Welcome to</h3> */}
          <p id="HitHubIntro">HITHUB</p>
          <p id="Explore">Explore the Top 50 Hits!</p>
        </div>
      </div>
    </div>
  );
}

export default SplashPage;
