import React, { useState, useEffect } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import "../styles/SearchDropdown.css";
import SearchDropdown from "../components/SearchDropdown";
import Search from "../components/search";
import SongCard from "../components/SongCard";
import ArtistCard from "../components/ArtistCard";
import GenreCard from "../components/GenreCard";
import { deployed_url, local_url } from "../components/axios_url";
import PaginationBar from "../components/PaginationBar";

function checkQueryInCard(card: any, query: string) {
  const ignoreKeys: string[] = ['artist_id', 'genre_id', 'song_link', 'song_ids', 'artist_link', 'artist_ids', 'song_ids'];

  for (let key in card.back) {
    if (!ignoreKeys.includes(key)) {
      const value = card.back[key];
      if (typeof value === 'string' && value.toLowerCase().includes(query.toLowerCase())) {
        return true;
      } else if (typeof value === 'number' && value.toString().includes(query)) {
        return true;
      } else if (Array.isArray(value)) {
        if (value.some((item: any) => typeof item === 'string' && item.toLowerCase().includes(query.toLowerCase()))) {
          return true;
        } else if (value.some((item: any) => typeof item === 'number' && item.toString().includes(query))) {
          return true;
        }
      }
    }
  }

  return false;
}

const SearchPage = () => {
  const [sortOrder, setSortOrder] = useState<string>("Sort Order");
  const [query, setQuery] = useState<string>("");
  const [cardArr, setCardArr] = useState<any[]>([]);
  const [pageIndex, setPageIndex] = useState(0);
  const numPerPage: number = 15;
  const [loading, setLoading] = useState<boolean>(true);
  const [newArr, setNewArr] = useState<any[]>([]);
  const cardArrLength: number = newArr.length;

  const handleSearch = (searchQuery: string) => {
    setQuery(searchQuery);
  };

  const changePage = (newIndex: number) => {
    setPageIndex(newIndex);
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await initializeCardArr();
      setCardArr(data);
      setLoading(false);
    };
    
    fetchData();
  }, []);

  useEffect(() => {
    let filteredArray = [...cardArr];

    if (query !== '') {
      filteredArray = cardArr.filter(card => checkQueryInCard(card, query));
    }

    // Sort filteredArray based on sortOrder and name field
    const sortedArray = filteredArray.sort((a, b) => {
      const nameA = a.back.name.toUpperCase();
      const nameB = b.back.name.toUpperCase();
      
      if (sortOrder === 'Descending') {
        return nameB.localeCompare(nameA);
      } else {
        return nameA.localeCompare(nameB);
      }
    });

    setNewArr(sortedArray);
  }, [query, sortOrder, cardArr]);

  if (loading) return (<p>Loading...</p>);
  
  let tempCardID = 0;
  
  return (
    <div className="container">
      <div data-bs-theme="glass" className="search-row">
        <Search onSearch={handleSearch} />
        <SearchDropdown searchType={sortOrder} handleSwitch={setSortOrder} items={["Ascending", "Descending"]}/>
      </div>
        <div className="row">
            {
                newArr.slice(numPerPage * pageIndex, numPerPage * pageIndex + numPerPage).map((card) => (
                    ('rank' in card.front &&
                        <div className="col col-custom-sm col-custom-md col-custom-lg">
                        <SongCard
                        key={tempCardID++}
                        id={card.id}
                        variant={card.variant}
                        front={card.front}
                        back={card.back}
                        query={query}
                        filterType="All"
                        /></div>)
                    || ('bio' in card.back &&
                        <div className="col col-custom-sm col-custom-md col-custom-lg"><ArtistCard
                        key={tempCardID++}
                        id={card.id}
                        variant={card.variant}
                        front={card.front}
                        back={card.back}
                        query={query}
                        filterType="All"
                        /></div>)
                    || ('origin_date' in card.back &&
                        <div className="col col-custom-sm col-custom-md col-custom-lg"><GenreCard
                        key={tempCardID++}
                        id={card.id}
                        variant={card.variant}
                        front={card.front}
                        back={card.back}
                        query={query}
                        filterType="All"
                        /></div>)
                ))
            }
        </div>
        <div>
            <PaginationBar pageIndex={pageIndex} changePage={changePage} dataLength={cardArrLength} numPerPage={numPerPage} />
        </div>
    </div>
  );
};

interface SongCard {
  id: string;
  variant: "hover" | "click";
  front: {
    rank: number;
    name: string;
    image: string;
  };
  back: {
    rank: number;
    name: string;
    artist: number;
    genre: number;
    duration: string;
    artist_id: number;
    genre_id: number;
    song_link: string;
  };
}
interface ArtistCard {
  id: string;
  variant: "hover" | "click";
  front: {
    name: string;
    image: string;
  };
  back: {
    name: string;
    bio: string;
    followers: number;
    genre: string;
    genre_id: number;
    popularity: string;
    songs: string[];
    song_ids: number[];
    artist_link: string;
  };
}
interface GenreCard {
  id: string;
  variant: "hover" | "click";
  front: {
    name: string;
  };
  back: {
    name: string;
    origin_date: string;
    origin_location: string;
    songs: string[];
    artists: string[];
    artist_ids: number[];
    song_ids: number[];
  };
}

async function initializeCardArr() {
  const data: any[] = (await local_url.get('/api/GetAll')).data;
  // const cardArr: any[] = [];
  let temp: any[] = [];
  for (let i = 0; i < data.length; i++ ) {
    if ('rank' in data[i]) {
      const newCard: SongCard = {
        id: data[i].rank.toString(),
        variant: "click",
        front: {
          rank: data[i].rank,
          name: data[i].name,
          image: data[i].image,
        },
        back: {
          rank: data[i].rank.toString(),
          name: data[i].name,
          artist: data[i].artist_name,
          genre: data[i].genre_name,
          duration: data[i].duration,
          artist_id: data[i].artist_id,
          genre_id: data[i].genre_id,
          song_link: data[i].spotify_page
        },
      };

      temp.push(newCard);
    }
    else if ('biography' in data[i]) {
      const newCard: ArtistCard = {
        id: data[i].id.toString(),
        variant: "click",
        front: {
          name: data[i].name,
          image: data[i].image,
        },
        back: {
          name: data[i].name,
          bio: data[i].biography,
          followers: data[i].followers.toString(),
          genre: data[i].genre_name,
          genre_id: data[i].genre_id,
          songs: data[i].songs,
          song_ids: data[i].song_ids,
          popularity: data[i].popularity,
          artist_link: data[i].spotify_page
        }
      };

      temp.push(newCard);
    }
    else if ('origin_date' in data[i]) {
      const newCard: GenreCard = {
        id: data[i].toString(),
        variant: "click",
        front: {
          name: data[i].name,
        },
        back: {
          name: data[i].name,
          origin_date: data[i].origin_date,
          origin_location: data[i].origin_location,
          songs: data[i].songs,
          artists: data[i].artists,
          artist_ids: data[i].artist_ids,
          song_ids: data[i].song_ids,
        },
      };

      temp.push(newCard);
    } 
  }

  return temp;
}

export default SearchPage;