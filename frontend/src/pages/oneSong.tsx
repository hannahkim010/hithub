import React, { useState, useEffect } from "react";
import {Link, useParams} from "react-router-dom";
import "../styles/oneSong.css";
import { deployed_url, local_url } from "../components/axios_url"; 
import spotifyLogo from "../components/spotifyLogo.png"

function OneSong() {
  const { id } = useParams();
  const [songData, setSongData] = useState({
    name: "",
    image: "",
    rank: 0,
    duration: "",
    genre_name: "",
    genre_id: 0,
    artist_name: "",
    artist_id: 0,
    spotify_page: ""
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const song = (await local_url.get(`/api/GetSong?song_id=${Number(id)}`)).data;
        setSongData(song);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [id]);

  const { name, image, rank, duration, genre_name, genre_id, artist_name, artist_id, spotify_page } = songData;

  return (
    <div className="container-one-thing">
      <div className="div-left">
        <img src={image} alt-text="song cover"></img>
      </div>
      <div className="div-right">
        <h1><b>{name}</b></h1>
        <h3><b> - Genre: </b><Link to={`/oneGenre/${genre_id}`} style={{ color: 'black' }}>{genre_name}</Link> </h3> 
        <h3><b> - Rank: </b>{rank}</h3>
        <h3><b> - Duration: </b>{duration}</h3>
        <h3><b> - Artist: </b><Link to= {`/oneArtist/${artist_id}`} style={{ color: 'black' }}>{artist_name}</Link></h3>
        <div className="spotify-logo-container-75">
          <Link to={spotify_page}>
            <img src={spotifyLogo} alt="Spotify" style={{ width: '50px', height: '50px' }} />
          </Link>
        </div>
      </div>

    </div>
  );
}
export default OneSong;
