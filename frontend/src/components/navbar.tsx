import { useState } from "react";
import { Link } from "react-router-dom";
import "../styles/navbar.css";

interface NavbarProps {
    brandName: string;
    imageSrcPath: string;
}

export default function Navbar({ brandName, imageSrcPath }: NavbarProps) {
    const [selectedIndex, setSelectedIndex] = useState(-1);

    const handleItemClick = (index: number) => {
        setSelectedIndex(index);
    };

    return (
        <nav className="navbar navbar-expand-md navbar-dark custom-navbar shadow fixed-top">
            <div className="container-fluid">
                <Link to="/" className="navbar-brand" onClick={() => handleItemClick(-1)}>
                    <img
                        src={imageSrcPath}
                        width="auto"
                        height="40"
                        className="d-inline-block align-center"
                        alt=""
                    />
                    <span className="fw-bolder fs-4">{brandName}</span>
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon" />
                </button>
                <div
                    className="collapse navbar-collapse align-items-center flex-column flex-md-row"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav me-auto mb-2 mb-md-1">
                        <li className="nav-item">
                            <Link
                                to="/songs"
                                className={
                                    selectedIndex === 0
                                        ? "nav-link active fw-bold"
                                        : "nav-link"
                                }
                                onClick={() => handleItemClick(0)}
                            >
                                Top 50
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                to="/artists"
                                className={
                                    selectedIndex === 1
                                        ? "nav-link active fw-bold"
                                        : "nav-link"
                                }
                                onClick={() => handleItemClick(1)}
                            >
                                Artists
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                to="/genres"
                                className={
                                    selectedIndex === 2
                                        ? "nav-link active fw-bold"
                                        : "nav-link"
                                }
                                onClick={() => handleItemClick(2)}
                            >
                                Genres
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                to="/about"
                                className={
                                    selectedIndex === 3
                                        ? "nav-link active fw-bold"
                                        : "nav-link"
                                }
                                onClick={() => handleItemClick(3)}
                            >
                                About
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link
                                to="/search"
                                className={
                                    selectedIndex === 4
                                        ? "nav-link active fw-bold"
                                        : "nav-link"
                                }
                                onClick={() => handleItemClick(4)}
                            >
                                Search
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}
