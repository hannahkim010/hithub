import { useState } from "react";
import cn from "classnames";
import "../styles/card.css";
import LearnButton from "./LearnButton";
import {Link} from "react-router-dom";
import Highlighter from "react-highlight-words";
import spotifyLogo from "./spotifyLogo.png";

interface CardProps {
  id: string;
  variant: "hover" | "click";
  front: {
    rank: number;
    name: string;
    image: string;
  };
  // back: string;
  back: {
    rank: number;
    name: string;
    artist: string;
    genre: string;
    duration: string;
    artist_id: number;
    genre_id: number;
    song_link: string;
  };
  query: string;
  filterType: string;
}

// component receiving props 'card'
function SongCard({ id, variant, front, back, query, filterType }: CardProps) {
  // tracks whether back of card shown
  const [showBack, setShowBack] = useState(false);

  // func handling click events on card
  function handleClick() {
    // if card variant is 'click', toggle state to show/hide card back
    if (variant === "click") {
      setShowBack(!showBack);
    }
  }

  return (
    <div
      className="flip-card-outer" // entire flip card
      onClick={handleClick} // call handleClick function on click
    >
      <div
        className={cn("flip-card-inner", {
          // flipping animations div
          showBack, // applies 'showBack' class if state is T
          "hover-trigger": variant === "hover", // applies 'hover-trigger' class if variant is 'hover'
        })}
      >
        {/* card front face */}
        <div className="card front">
          <div className="img-box">
            <img src={front.image} alt={front.name} className="card-img-top" />
          </div>

          {/* flexbox layout (centers content) */}
          <div className="card-body d-flex justify-content-center align-items-center">
            {/* text content of front */}
            {
              filterType === 'Songs' ?
                <p className='className="card-text fs-4 fw-bold'>
                  {front.rank.toString() + ') '}<Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={front.name}/>
                </p> :
              filterType === 'Filter' || filterType === 'All' ?
                <p className='className="card-text fs-4 fw-bold'>
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={front.rank.toString()}/>
                    {') '}
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={front.name}/>
                </p> :
                <p className='className="card-text fs-4 fw-bold'>{`${front.rank}) ${front.name}`}</p>
            }
          </div>
        </div>
        {/* same thing but for back face */}
        <div className="card back">
          {/* <div className="card-body d-flex justify-content-center align-items-center"> */}
          <div className="card-body">
            <p className="card-name fs-1 fw-bold">
            {
              filterType === 'All' || filterType === 'Filter' || filterType === 'Songs' ? 
                <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.name}/> :
                back.name
            }
            </p>
            <p className="card-text-info">
              <b>Rank: </b> 
                {
                  filterType === 'All' || filterType === 'Filter' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.rank.toString()}/> :
                    back.rank
                }
              <br />
              <b>Artist: </b>
              <Link to= {`/oneArtist/${back.artist_id}`} style={{ color: 'black' }}> 
                {
                  filterType === 'All' || filterType === 'Filter' || filterType === 'Artists' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.artist}/> :
                    back.artist
                }
              </Link>
              <br />
              <b>Genre: </b>
              <Link to= {`/oneGenre/${back.genre_id}`} style={{ color: 'black' }}> 
                { 
                  filterType === 'All' || filterType === 'Filter' || filterType === 'Genres' ? 
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.genre}/> :
                  back.genre
                }
              </Link>
              <br />
              <b>Duration:</b> 
                {
                  filterType === 'All' || filterType === 'Filter' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.duration}/> :
                    back.duration
                }
              <br />
              <br />
              <div className="spotify-logo-container">
                <Link to={back.song_link}>
                  <img src={spotifyLogo} alt="Spotify" style={{ width: '50px', height: '50px' }} />
                </Link>
              </div>
            </p>
          </div>
          <div className="d-flex justify-content-center mb-4">
            <LearnButton string_link="/oneSong" id={id}></LearnButton>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SongCard;
