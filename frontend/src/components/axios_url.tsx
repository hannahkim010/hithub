import axios from 'axios';

export const local_url = axios.create({
  baseURL: 'http://127.0.0.1'
});

export const deployed_url = axios.create({
  baseURL: 'https://backend-dot-cs-373-hithub.uc.r.appspot.com'
});