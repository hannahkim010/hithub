import React, { useState } from "react";
import cn from "classnames";
import "../styles/card.css";
import LearnButton from "./LearnButton";
import { Link } from "react-router-dom";
import Highlighter from "react-highlight-words";

interface CardProps {
  id: string;
  variant: "hover" | "click";
  front: {
    name: string;
  };
  back: {
    name: string;
    origin_date: string;
    origin_location: string;
    songs: string[];
    artists: string[];
    artist_ids: number[];
    song_ids: number[];
  };
  query: string;
  filterType: string;
}

// component receiving props 'card'
function GenreCard({ id, variant, front, back, query, filterType }: CardProps) {
  // tracks whether back of card shown
  const [showBack, setShowBack] = useState(false);

  // func handling click events on card
  function handleClick() {
    // if card variant is 'click', toggle state to show/hide card back
    if (variant === "click") {
      setShowBack(!showBack);
    }
  }

  return (
    <div
      className="flip-card-outer" // entire flip card
      onClick={handleClick} // call handleClick function on click
    >
      <div
        className={cn("flip-card-inner", {
          // flipping animations div
          showBack, // applies 'showBack' class if state is T
          "hover-trigger": variant === "hover", // applies 'hover-trigger' class if variant is 'hover'
        })}
      >
        {/* card front face */}
        <div className="card front">
          {/* <img src={front.image} alt={front.name} className="card-img-top" /> */}
          {/* flexbox layout (centers content) */}
          <div className="card-body d-flex justify-content-center align-items-center">
            {/* text content of front */}
            <p className="card-text fw-bold fs-2 style={{ fontSize: '100px', fontWeight: 'bold' }}">
              {
                filterType === 'All' || filterType === 'Filter' || filterType === 'Genres' ? 
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={front.name}/> :
                  front.name
              }
            </p>
          </div>
        </div>
        {/* same thing but for back face */}
        <div className="card back">
          {/* <div className="card-body d-flex justify-content-center align-items-center"> */}
          <div className="card-body">
            <p className="card-title fs-1 fw-bold">
              {
                filterType === 'All' || filterType === 'Filter' || filterType === 'Genres' ? 
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.name}/> :
                  back.name
              }
            </p>
            <p className="card-text-artist">
              <b>Origin Location: </b>
                {
                filterType === 'All' || filterType === 'Filter' ? 
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.origin_location}/> :
                  back.origin_location
                }
              <br />
              <b>Origin Date: </b> 
                {
                  filterType === 'All' || filterType === 'Filter' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.origin_date}/> :
                    back.origin_date
                }
              <br />
              <b>Artists: </b>
              {back.artists.map((artist, index) => (
                <React.Fragment key={index}>
                  <Link to={`/oneArtist/${back.artist_ids[index]}`} style={{ color: 'black' }}>
                    {
                      filterType === 'All' || filterType === 'Filter' || filterType === 'Artists' ? 
                      <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={artist}/> :
                      artist
                    }
                  </Link>
                  {index !== back.artists.length - 1 && ", "}
                </React.Fragment>
              ))}{" "}
              <br />
              <b>Songs: </b>
              {back.songs.map((song, index) => (
                <React.Fragment key={index}>
                  <Link to={`/oneSong/${back.song_ids[index]}`} style={{ color: 'black' }}>
                  {
                    filterType === 'All' || filterType === 'Filter' || filterType === 'Songs' ? 
                      <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={song}/> :
                      song
                  }
                  </Link>
                  {index !== back.songs.length - 1 && ", "}
                </React.Fragment>
              ))}
            </p>
          </div>
          <div className="d-flex justify-content-center mb-4">
            <LearnButton string_link="/oneGenre" id={id}></LearnButton>
          </div>
        </div>
      </div>
    </div>
  );
}

export default GenreCard;
