import { useEffect, useState } from "react";
import "../styles/Pagination.css"

interface PaginationBarProps {
  pageIndex: number;
  changePage: (newIndex: number) => void;
  dataLength: number;
  numPerPage: number;
}
const PaginationBar = ({pageIndex, changePage, dataLength, numPerPage}: PaginationBarProps) => {
  const pageArr = [];
  const limit = dataLength % numPerPage == 0 ? dataLength / numPerPage - 1 : Math.floor(dataLength / numPerPage);

  for (let i = 0; i <= limit; i++) {
    pageArr.push(
      <li 
        className={pageIndex == i ? "page-item active" : "page-item"}
        onClick={() => changePage(i)}
      >
        <a className={pageIndex == i ? "page-link pagination-active" : "page-link pagination-color"}>{i + 1}</a>
      </li>
    )
  }

  const [prevDisabled, setPrevDisabled] = useState<boolean>(false);
  const [nextDisabled, setNextDisabled] = useState<boolean>(false);

  useEffect(() => {
    setPrevDisabled(pageIndex <= 0);
    setNextDisabled(pageIndex >= limit)
  }, [pageIndex, dataLength])

  return (
    <nav aria-label="Pages for chunks of cards">
      <ul className="pagination">
        {!prevDisabled && 
          <li 
          className="page-item"
          onClick={() => changePage(pageIndex - 1)}
          >
            <a className="page-link pagination-color">Previous</a>
          </li>
        }
        {pageArr}
        {!nextDisabled &&
          <li 
          className="page-item"
          onClick={() => changePage(pageIndex + 1)}
          > 
            <a className="page-link pagination-color">Next</a>
          </li>
        }
      </ul>
    </nav>
  )
}

export default PaginationBar;