import React from "react";
import YouTube from "react-youtube";

interface Props {
  videoID: string;
}

function YouTubeVideo({videoID} : Props) {
  const opts = {
    width: '640',
    height: '360',
  };
  return (
    <div>
    <YouTube videoId={videoID} opts={opts} />
  </div>

  );
}

export default YouTubeVideo;
