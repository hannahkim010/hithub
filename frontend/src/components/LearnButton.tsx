// import { Link } from "react-router-dom";
// import "../styles/LearnButton.css";

// interface Props {
//     string_link: string;
//     id: string;
//   }
//   const LearnButton = ({string_link, id}: Props) => {
//     return (
//       <button className="btn btn-outline-light">
//         <Link to={`${string_link}/${id}`}>Learn More</Link>
//       </button>
//     );
//   };
  
// export default LearnButton;


import React from "react";
import { Link } from "react-router-dom";
import "../styles/LearnButton.css";

interface Props {
    string_link: string;
    id: string;
}

const LearnButton = ({ string_link, id }: Props) => {
    return (
        <button className="btn learn">
            <Link to={`${string_link}/${id}`}>Learn More</Link>
        </button>
    );
};

export default LearnButton;
