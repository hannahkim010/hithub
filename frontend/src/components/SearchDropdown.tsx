import { Dropdown, DropdownButton } from "react-bootstrap";
import "../styles/SearchDropdown.css";

interface SearchDropdownProps {
  searchType: string;
  handleSwitch: (type: string) => void;
  items: string[];
}

const SearchDropdown = ({
  searchType,
  handleSwitch,
  items,
}: SearchDropdownProps) => {
  const itemArr = items.map((item, index) => {
    if (item === "None") {
      return (
        <Dropdown.Item
          key={`dropdown-item-none-${index}`}
          as="button"
          className="dropdown-item"
          onClick={() => handleSwitch("Sort By")}
        >
          {item}
        </Dropdown.Item>
      );
    }
    return (
      <Dropdown.Item
        key={`dropdown-item-${index}`}
        as="button"
        className="dropdown-item"
        onClick={() => handleSwitch(item)}
      >
        {item}
      </Dropdown.Item>
    );
  });

  return (
    <div className="dropdown">
      <DropdownButton
        title={searchType}
        id="sort-btn"
        variant="transparent"
        color="white"
      >
        {itemArr}
      </DropdownButton>
    </div>
  );
};

export default SearchDropdown;
