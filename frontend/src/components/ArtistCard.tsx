import React,{ useState } from "react";
import cn from "classnames";
import "../styles/card.css";
import LearnButton from "./LearnButton";
import { Link } from "react-router-dom";
import Highlighter from "react-highlight-words";
import spotifyLogo from "./spotifyLogo.png"

interface CardProps {
  id: string;
  variant: "hover" | "click";
  front: {
    name: string;
    image: string;
  };
  back: {
    name: string;
    bio: string;
    followers: number;
    genre: string;
    genre_id: number;
    popularity: number;
    songs: string[];
    song_ids: number[];
    artist_link: string;
  };
  query: string;
  filterType: string;
}

// component receiving props 'card'
function ArtistCard({ id, variant, front, back, query, filterType }: CardProps) {
  // tracks whether back of card shown
  const [showBack, setShowBack] = useState(false);

  // func handling click events on card
  function handleClick() {
    // if card variant is 'click', toggle state to show/hide card back
    if (variant === "click") {
      setShowBack(!showBack);
    }
  }

  return (
    <div
      className="flip-card-outer" // entire flip card
      onClick={handleClick} // call handleClick function on click
    >
      <div
        className={cn("flip-card-inner", {
          // flipping animations div
          showBack, // applies 'showBack' class if state is T
          "hover-trigger": variant === "hover", // applies 'hover-trigger' class if variant is 'hover'
        })}
      >
        {/* card front face */}
        <div className="card front">
          <div className="img-box-artist">
            <img src={front.image} alt={front.name} className="card-img-top-artist" />
          </div>
          {/* flexbox layout (centers content) */}
          <div className="card-body d-flex justify-content-center align-items-center">
            {/* text content of front */}
            <p className="card-text fs-3 fw-bold style={{ fontSize: '100px', fontWeight: 'bold' }}">
              {
                filterType === 'All' || filterType === 'Filter' || filterType === 'Artists' ? 
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={front.name}/> :
                  front.name
              }
            </p>
          </div>
        </div>
        {/* same thing but for back face */}
        <div className="card back">
          {/* <div className="card-body d-flex justify-content-center align-items-center"> */}
          <div className="card-body">
            <p className="card-title fs-1 fw-bold">
              {
                filterType === 'All' || filterType === 'Filter' || filterType === 'Artists' ? 
                  <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.name}/> :
                  back.name
              }
            </p>
            <p className="card-text-artist">
              {/* <b>Biography:</b> {back.bio}<br /> */}
              <b>Followers: </b>
                {
                  filterType === 'All' || filterType === 'Filter' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.followers.toString()}/> :
                    back.followers.toString()
                }
              <br />
              <b>Genre: </b> 
              <Link to={`/oneGenre/${back.genre_id}`} style={{ color: 'black' }}>
                {
                  filterType === 'All' || filterType === 'Filter' || filterType === 'Genres' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.genre}/> :
                    back.genre
                }
              </Link><br />
              <b>Popularity Score: </b>
                {
                  filterType === 'All' || filterType === 'Filter' ? 
                    <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={back.popularity.toString()}/> :
                    back.popularity.toString()
                }
              <br/>
              <b>Songs: </b>
              {back.songs.map((song, index) => (
                <React.Fragment key={index}>
                  <Link to={`/oneSong/${back.song_ids[index]}`} style={{ color: 'black' }}>
                    {
                      filterType === 'All' || filterType === 'Filter' || filterType === 'Songs' ? 
                        <Highlighter highlightClassName='highlight' searchWords={[query]} textToHighlight={song}/> :
                        song
                    }
                  </Link>
                  {index !== back.songs.length - 1 && ", "}
                </React.Fragment>
              ))} <br /> <br/>
              <div className="spotify-logo-container">
                <Link to={back.artist_link} style={{ color: 'black' }}>
                  <img src={spotifyLogo} alt="Spotify" style={{ width: '50px', height: '50px' }} />
                </Link>
              </div>
            </p>
          </div>
          <div className="d-flex justify-content-center mb-4">
            <LearnButton string_link="/oneArtist" id={id}></LearnButton>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ArtistCard;
