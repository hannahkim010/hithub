// // import React, { useState } from "react";
// // import "../styles/search.css";

// // interface SearchProps {
// //   onSearch: (query: string) => void;
// // }

// // const Search = ({ onSearch }: SearchProps) => {
// //   const [query, setQuery] = useState("");

// //   const handleSearch = () => {
// //     if (query.trim() !== "") {
// //       onSearch(query);
// //     }
// //   };

// //   const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
// //     if (e.key === "Enter" && query.trim() !== "") {
// //       onSearch(query);
// //     }
// //   };

// //   return (
// //     <div className="glass-search-container">
// //       <input
// //         type="text"
// //         className="glass-search-input"
// //         value={query}
// //         onChange={(e) => setQuery(e.target.value)}
// //         onKeyPress={handleKeyPress}
// //         placeholder="Search..."
// //       />
// //       <button className="glass-search-button" onClick={handleSearch}>
// //         <img
// //           src="https://cdn0.iconfinder.com/data/icons/art-designing-glyph/2048/1871_-_Magnifier-512.png"
// //           alt="Search Icon"
// //         />
// //       </button>
// //     </div>
// //   );
// // };

// // export default Search;
// import React, { useState } from "react";
// import "../styles/search.css";

// interface SearchProps {
//   onSearch: (query: string) => void;
// }

// const Search = ({ onSearch }: SearchProps) => {
//   const [query, setQuery] = useState("");

//   const handleSearch = () => {
//     if (query.trim() !== "") {
//       onSearch(query);
//     }
//   };

//   const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
//     if (e.key === "Enter" && query.trim() !== "") {
//       onSearch(query);
//     }
//   };

//   return (
//     <div className="glass-search-container">
//       <input
//         type="text"
//         className="glass-search-input"
//         value={query}
//         onChange={(e) => setQuery(e.target.value)}
//         onKeyPress={handleKeyPress}
//         placeholder="Search..."
//       />
//       <button className="glass-search-button" onClick={handleSearch}>
//         <img
//           src="https://cdn0.iconfinder.com/data/icons/art-designing-glyph/2048/1871_-_Magnifier-512.png"
//           alt="Search Icon"
//         />
//       </button>
//     </div>
//   );
// };

// export default Search;

import React, { useState } from "react";
import "../styles/search.css";
import { IoIosSearch } from "react-icons/io";


interface SearchProps {
  onSearch: (query: string) => void;
}

const Search = ({ onSearch }: SearchProps) => {
  const [query, setQuery] = useState("");

  const handleSearch = () => {
    onSearch(query);
  };

  const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter" && query.trim() !== "") {
      onSearch(query);
    }
  };

  return (
    <div className="glass-search-container">
      <div className="glass-search-wrapper">
        <input
          type="text"
          className="glass-search-input"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          onKeyPress={handleKeyPress}
          placeholder="Search..."
        />
        <button className="glass-search-button" onClick={handleSearch}>
          <IoIosSearch />
        </button>
      </div>
    </div>
  );
};

export default Search;
