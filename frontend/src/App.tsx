import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import SplashPage from "./pages/SplashPage";
import Songs from "./pages/songs";
import Artists from "./pages/artists";
import Genres from "./pages/genres";
import About from "./pages/about";
import OneSong from "./pages/oneSong";
import OneArtist from "./pages/oneArtist";
import OneGenre from "./pages/oneGenre";
import Navbar from "./components/navbar";
import Search from "./components/search";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import imagePath from "./components/hithub_logo.png";
import SearchPage from "./pages/SearchPage";

// Adjust the path as per your file structure

function App() {
  return (
    <Router>
      <div>
        <Navbar brandName="HitHub" imageSrcPath={imagePath} />
      </div>
      <Routes>
        <Route path="/" element={<SplashPage />} />
        <Route path="/songs" element={<Songs />} />
        <Route path="/artists" element={<Artists />} />
        <Route path="/genres" element={<Genres />} />
        <Route path="/about" element={<About />} />
        <Route path="/oneSong/:id" element={<OneSong />} />
        <Route path="/oneArtist/:id" element={<OneArtist />} />
        <Route path="/oneGenre/:id" element={<OneGenre />} />
        <Route path="/search" element={<SearchPage />} />
      </Routes>
    </Router>
  );
}

export default App;
